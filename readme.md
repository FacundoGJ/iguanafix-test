# Test IguanaFix

## Versión productiva:

[www.dacu.com.ar/iguanafix/_dist/](http://www.dacu.com.ar/iguanafix/_dist/)

## Versión para debuggear js y css:

[www.dacu.com.ar/iguanafix/_debug/](http://www.dacu.com.ar/iguanafix/_debug/)


## Comandos por unica vez:
```
npm install
npm install gulp -g
```

## Comandos para release:
```
gulp release
```
## Comandos para `watch:Sass` y `watch:html` :
```
gulp
```