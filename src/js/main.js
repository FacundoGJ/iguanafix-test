$('nav.main li:not(.logo)')
  .click(function () {
    $('nav.main li').removeClass('active');
    $(this).addClass('active');
  });

$('body').click(function (e) {
  if ($(e.target).parent().hasClass('menu-secundary') || $(e.target).hasClass('menu-secundary')) {
    $('.main').toggleClass('expanded');
    $('.search-bar').toggleClass('expanded');
    $('.content-page').toggleClass('expanded');
  } else {
    $('.main').removeClass('expanded');
    $('.search-bar').removeClass('expanded');
    $('.content-page').removeClass('expanded');
  }
});